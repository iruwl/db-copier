from sqlalchemy import (
    create_engine,
    MetaData,
    Table,
    select,
    func,
    )
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker


Base = declarative_base()

class BaseModel(object):
    def to_dict(self):
        values = {}
        for column in self.__table__.columns:
            values[column.name] = getattr(self, column.name)
        return values
        
    def from_dict(self, values):
        for column in self.__table__.columns:
            if column.name in values:
                setattr(self, column.name, values[column.name])


class DBProfile(object):
    def __init__(self, db_url):
        self.engine = create_engine(db_url)
        self.metadata = MetaData(self.engine)
        base_session = sessionmaker(bind=self.engine)
        self.session = base_session()
        
    def query(self, *args):
        return self.session.query(*args)

    def flush(self, row=None):
        row and self.session.add(row)
        self.session.flush()
        
    def commit(self, row=None):
        self.flush(row)
        self.session.commit()

    def execute(self, sql, **kwargs):
        return self.engine.execute(sql, **kwargs)
    
    def first_value(self, sql):
        q = self.engine.execute(sql)
        row = q.first()
        return row[0]

    def get_table(self, tablename, schema=None):
        return Table(tablename, self.metadata, autoload=True, schema=schema)

    def get_orm(self, tablename, schema=None):
        class T(Base, BaseModel):
            __table__ = self.get_table(tablename, schema)
        return T

    def get_count(self, table):
        sql = select([func.count()]).select_from(table)
        q = self.execute(sql)
        return q.scalar()
