Database Copier
===============

Copy database structure and data use SQLAlchemy.

Edit ``conf.py`` then run::

  $ python batchmove.py

Happy migration.
