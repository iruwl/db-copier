# http://stackoverflow.com/questions/9593610/creating-a-temporary-table-from-a-query-using-sqlalchemy-orm
# http://stackoverflow.com/questions/6745189/how-do-i-get-the-name-of-an-sqlalchemy-objects-primary-key

import sys
import imp
from types import FloatType
from optparse import OptionParser
from sqlalchemy import (
    Column,
    Table,
    Integer,
    Float,
    DateTime,
    func,
    )
from sqlalchemy.exc import IntegrityError
from sqlalchemy.schema import (
    ForeignKeyConstraint,
    PrimaryKeyConstraint,
    )
from models import DBProfile


def column_info(c):
    if c.nullable:
        null_info = ''
    else:
        null_info = ' NOT NULL'
    if c.primary_key:
        pkey_info = ' *'
    else:
        pkey_info = ''
    return '{name} {type}{null}{pkey}'.format(
            name=c.name, type=c.type, null=null_info, pkey=pkey_info)


def change_column_def(c):
    if c.type.python_type == FloatType and hasattr(c.type, 'precision'):
        if c.type.precision > 10:
            return Column(Float, nullable=c.nullable, name=c.name)
        return Column(Integer, nullable=c.nullable, name=c.name)
    if hasattr(c.server_default, 'arg') and \
        hasattr(c.server_default.arg, 'text') and \
        c.server_default.arg.text.strip().lower() == 'sysdate': # Oracle
        return Column(DateTime, nullable=c.nullable, name=c.name,
            default=func.now)
    return c

def get_primary_key(t):
    k = []
    for c in t.columns:
        if c.primary_key:
            k.append(c.name)
    return k

def has_primary_key(t):
    for c in t.columns:
        if c.primary_key:
            return True

def get_first_fields(t, field_count=10):
    fields = []
    i = 0
    for c in t.columns:
        i += 1
        fields.append(c.name)
        if i > field_count:
            break
    return fields


class Info(object):
    def __init__(self, db_source, schema_source=None):
        self.db_source = db_source
        self.schema_source = schema_source
        
    def show_structure(self, Source):
        print('Structure {name}'.format(name=Source.name))
        cols = []
        is_pkey = has_primary_key(Source)
        if not is_pkey:
            seq_name = '{name}_id_seq'.format(name=Source.name)
            cols.append(Column('id', Integer, primary_key=True).copy())
        for c in Source.columns:
            c = change_column_def(c)
            cols.append(c.copy())
        for c in cols:
            print('  ' + column_info(c))
        constraints = []
        for c in Source.constraints:
            if c.__class__ == PrimaryKeyConstraint and not is_pkey:
                continue
            if c.__class__ == ForeignKeyConstraint:
                c = self.change_foreign_key(c)
                constraints.append(c)
            else:
                constraints.append(c.copy())
        for c in constraints:
            if c.__class__ == ForeignKeyConstraint:
                refs = [e.target_fullname for e in c.elements]
                print('  foreign key {c}'.format(c=c.columns))
                print('    references {ref}'.format(ref=refs))

    def show(self, tablename):
        tablenames = self.db_source.engine.table_names(self.schema_source)
        t = self.db_source.get_table(tablename, self.schema_source)
        if tablename in tablenames:
            self.show_structure(t)
        
    def change_foreign_key(self, constraint):
        references = []
        column_names = [column for column in constraint.columns]
        for e in constraint.elements:
            t = e.target_fullname.split('.')
            ref_fieldname = t[-1]
            ref_tablename = t[-2]
            ref = [ref_tablename, ref_fieldname]
            ref = '.'.join(ref)
            references.append(ref)
        return ForeignKeyConstraint(column_names, references)


conf_file = 'conf.py'
pars = OptionParser()
pars.add_option('-c', '--conf', default=conf_file,
        help='configuration file, default: ' + conf_file)
pars.add_option('-t', '--table')
option, remain = pars.parse_args(sys.argv[1:])

conf_file = option.conf
conf = imp.load_source('conf', conf_file)

db_source = DBProfile(conf.db_url_source)

info = Info(db_source, conf.schema_source)

if option.table:
    tablenames = option.table.split(',')
else:
    tablenames = conf.copy_tablenames
for tablename in conf.copy_tablenames:
    info.show(tablename)
