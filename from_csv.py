import os
import sys
import imp
import csv
from optparse import OptionParser
from sqlalchemy import PrimaryKeyConstraint
from models import (
    DBProfile,
    Base,
    BaseModel,
    )


# Get schema in tablename
def split_tablename(tablename):
    t = tablename.split('.')
    if t[1:]:
        return t[0], t[1]
    return None, tablename

def get_pkeys(table):
    r = []
    for c in table.constraints:
        if c.__class__ is PrimaryKeyConstraint:
            for col in c:
                r.append(col.name)
            return r
    return r


conf_file = 'conf.py'
pars = OptionParser()
pars.add_option('-c', '--conf', default=conf_file,
        help='configuration file, default: ' + conf_file)
pars.add_option('-i', '--input', help='CSV file')
pars.add_option('-t', '--table', help='restore to table')
option, remain = pars.parse_args(sys.argv[1:])

conf_file = option.conf
filename = option.input
schema, tablename = split_tablename(option.table)

conf = imp.load_source('conf', conf_file)
db_target = DBProfile(conf.db_url_target)
target_table = db_target.get_table(tablename, schema)
target_fields = []
for c in target_table.columns:
    target_fields.append(c.name)

class Target(Base, BaseModel):
    __table__ = target_table

f = open(filename)
reader = csv.DictReader(f)
for source in reader:
    break
f.close()

source2target_fields = dict()
target2source_fields = dict()
for source_field in source:
    if source_field in target_fields:
        target_field = source_field
    elif source_field.lower() in target_fields:
        target_field = source_field.lower()
    elif source_field.upper() in target_fields:
        target_field = source_field.upper()
    else:
        target_field = None
    if target_field:
        source2target_fields[source_field] = target_field
        target2source_fields[target_field] = source_field
        print('{s} -> {t}'.format(s=source_field, t=target_field))
    else:
        print('{s} does not exists on {t}'.format(
            s=source_field, t=option.table))

target_keys = get_pkeys(target_table)
f = open(filename)
reader = csv.DictReader(f)
for source in reader:
    filter_ = {}
    for target_key in target_keys:
        source_key = target2source_fields[target_key]
        val = source[source_key]
        filter_[target_key] = val
    q = db_target.query(Target).filter_by(**filter_)
    if q.first():
        continue
    target_data = dict()
    for source_field in source:
        if source_field not in source2target_fields:
            continue
        val = source[source_field]
        target_field = source2target_fields[source_field]
        target_data[target_field] = val
    target = Target()
    target.from_dict(target_data)
    db_target.commit(target)
f.close()
