import os
import sys
import imp
import csv
from optparse import OptionParser
from sqlalchemy import create_engine


conf_file = 'conf.py'
filename = 'output.csv'
pars = OptionParser()
pars.add_option('-c', '--conf', default=conf_file,
        help='configuration file, default: ' + conf_file)
pars.add_option('', '--sql', help='query')
pars.add_option('', '--output', default=filename,
        help='save to file, default ' + filename)
option, remain = pars.parse_args(sys.argv[1:])

conf_file = option.conf
sql = option.sql
if os.path.exists(sql):
    sql = open(sql).read()
filename = option.output
conf = imp.load_source('conf', conf_file)

fieldnames = []
engine = create_engine(conf.db_url_source)
q = engine.execute(sql)
f = csv.writer(open(filename, 'wb'))
for row in q:
    if not fieldnames:
        fieldnames = row.keys()
        f.writerow(fieldnames)
    vals = []
    for fieldname in fieldnames:
        val = row[fieldname]
        vals.append(val)
    f.writerow(vals)
