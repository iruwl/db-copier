# http://stackoverflow.com/questions/9593610/creating-a-temporary-table-from-a-query-using-sqlalchemy-orm
# http://stackoverflow.com/questions/6745189/how-do-i-get-the-name-of-an-sqlalchemy-objects-primary-key

import os
import sys
import imp
import locale
from string import printable
from types import (
    FloatType,
    StringType,
    UnicodeType,
    IntType,
    LongType,
    )
StringTypes = (StringType, UnicodeType)
BooleanTypes = ('bit',)
IntTypes = (IntType, LongType)
FloatTypes = ('money',)
from optparse import OptionParser
from time import time
from datetime import datetime
from sqlalchemy import (
    Column,
    Table,
    String,
    Integer,
    Float,
    DateTime,
    Boolean,
    func,
    )
from sqlalchemy.exc import IntegrityError
from sqlalchemy.schema import ForeignKeyConstraint
from models import (
    DBProfile,
    Base,
    BaseModel,
    )


def humanize_time(secs):
    mins, secs = divmod(secs, 60)
    hours, mins = divmod(mins, 60)
    return '%02d:%02d:%02d' % (hours, mins, secs)

def get_target_name(source_name):
    if conf.lowercase_target:
        return source_name.lower()
    return source_name

def clean_value(v):
    for a, b in conf.replace_chars:
        v = v.replace(a, b)
    if not conf.printable_chars_only:
        return v
    r = ''
    for ch in v:
        if ch in printable:
            r += ch
        else:
            r += ' '
    return r 

def get_dict(source):
    dict_source = dict(source)
    dict_target = dict()
    for key in dict_source:
        fname = get_target_name(key)
        value = dict_source[key]
        if type(value) in StringTypes:
            value = clean_value(value)
        dict_target[fname] = value
    return dict_target

def to_dict(t):
    i = -1
    r = dict()
    for fname in t.keys():
        i += 1
        r[fname] = t[i]
    return r

def get_target_columns(source_columns):
    target_columns = []
    for name in source_columns: 
        fname = get_target_name(name)
        target_columns.append(fname)
    return target_columns

def column_info(c):
    if c.nullable:
        null_info = ''
    else:
        null_info = ' NOT NULL'
    if c.primary_key:
        pkey_info = ' *'
    else:
        pkey_info = ''
    return '{name} {type}{null}{pkey}'.format(
            name=c.name, type=c.type, null=null_info, pkey=pkey_info)

def change_column_def(c):
    fieldname = get_target_name(c.name)
    orig_type = str(c.type).lower()
    if orig_type in BooleanTypes:
        return Column(Boolean, nullable=c.nullable, name=fieldname,
                primary_key=c.primary_key)
    if orig_type in FloatTypes:
        return Column(Float, nullable=c.nullable, name=fieldname,
                primary_key=c.primary_key)
    if c.type.python_type in StringTypes:
        return Column(String, nullable=c.nullable, name=fieldname,
                primary_key=c.primary_key)
    if c.type.python_type in IntTypes:
        return Column(Integer, nullable=c.nullable, name=fieldname,
                primary_key=c.primary_key)
    if c.type.python_type == FloatType and hasattr(c.type, 'precision'):
        if c.type.precision > 10:
            return Column(Float, nullable=c.nullable, name=fieldname,
                    primary_key=c.primary_key)
        return Column(Integer, nullable=c.nullable, name=fieldname,
                primary_key=c.primary_key)
    if hasattr(c.server_default, 'arg') and \
        hasattr(c.server_default.arg, 'text') and \
        c.server_default.arg.text.strip().lower() == 'sysdate': # Oracle
        return Column(DateTime, nullable=c.nullable, name=fieldname,
                primary_key=c.primary_key, default=func.now)
    if c.type.python_type is datetime:
        return Column(DateTime, nullable=c.nullable, name=fieldname,
                primary_key=c.primary_key)
    return Column(c.type, nullable=c.nullable, name=fieldname,
            primary_key=c.primary_key)

def get_primary_key(t):
    k = []
    for c in t.columns:
        if c.primary_key:
            k.append(c.name)
    return k

def has_primary_key(t):
    for c in t.columns:
        if c.primary_key:
            return True

def get_first_fields(t, field_count=10):
    fields = []
    i = 0
    for c in t.columns:
        i += 1
        fields.append(c.name)
        if i > field_count:
            break
    return fields

def load_plugin(filename):
    module_name = os.path.split(filename)[-1].split('.')[0]
    return imp.load_source(module_name, filename)

# Get schema in tablename
def split_tablename(tablename):
    t = tablename.split('.')
    if t[1:]:
        return t[0], t[1]
    return None, tablename

def print_debug(msg):
    if debug:
        print('DEBUG: {s}'.format(s=msg))

def thousand(n):
    return locale.format('%.0f', n, True)

def float_thousand(n):
    return locale.format('%.2f', n, True)


class BatchMove(object):
    def __init__(self, db_source, db_target,
                  schema_source=None, schema_target=None):
        self.db_source = db_source
        self.db_target = db_target
        self.schema_source = schema_source
        self.schema_target = schema_target
        self.plugin_names = []
        self.plugins = []
        
    def copy_structure(self, Source):
        print('Structure {name}'.format(name=Source.name))
        cols = []
        is_pkey = has_primary_key(Source)
        if not is_pkey:
            seq_name = '{name}_id_seq'.format(name=Source.name)
            cols.append(Column('id', Integer, primary_key=True).copy())
        for c in Source.columns:
            c = change_column_def(c)
            cols.append(c.copy())
        for c in cols:
            print('  ' + column_info(c))
        constraints = []
        for c in Source.constraints:
            if c.__class__ == ForeignKeyConstraint:
                c = self.change_foreign_key(c)
                constraints.append(c)
                refs = [e.target_fullname for e in c.elements]
                print('  foreign key {c}'.format(c=c.columns))
                print('    references {ref}'.format(ref=refs))
        tablename_target = get_target_name(Source.name)
        Target = Table(tablename_target, self.db_target.metadata,
                        schema=self.schema_target,
                        *(cols + constraints))
        Target.create()

    def copy_data(self, Source):
        count = self.db_source.get_count(Source)
        print('Count {name} {count} record'.format(name=Source.name,
            count=count))
        order_by = get_primary_key(Source)
        if not order_by:
            order_by = get_first_fields(Source)
        order_by = ', '.join(order_by)
        sql_source = Source.select().order_by(order_by)
        Target = self.db_target.get_table(get_target_name(Source.name),
                    self.schema_target)
        i = offset = self.db_target.get_count(Target)
        found = True
        current_count = 0
        begin_time = time()
        last_estimate = None
        while found:
            found = False
            sql = sql_source.offset(offset).limit(conf.limit)
            sources = self.db_source.execute(sql)
            for source in sources:
                found = True
                data = get_dict(source)
                sql = Target.insert().values(data)
                self.insert(sql)
                i += 1
                current_count += 1
                remain = count - i
                duration = time() - begin_time
                speed = duration / current_count
                estimate = int(speed * remain)
                if estimate != last_estimate:
                    last_estimate = estimate
                    e = humanize_time(estimate)
                    print('{t} {row} / {count} estimate {e}'.format(
                        t=Source.name, row=i, count=count, e=e))
            offset += conf.limit

    def copy(self, tablename):
        target_tablenames = self.db_target.engine.table_names(self.schema_target)
        t = self.db_source.get_table(tablename, self.schema_source)
        if get_target_name(tablename) not in target_tablenames:
            self.copy_structure(t)
        self.copy_data(t)
        
    def change_foreign_key(self, constraint):
        references = []
        column_names = [column for column in constraint.columns]
        for e in constraint.elements:
            t = e.target_fullname.split('.')
            ref_fieldname = t[-1]
            ref_tablename = t[-2]
            ref = [ref_tablename, ref_fieldname]
            if self.schema_target:
                ref = [self.schema_target] + ref
            ref = '.'.join(ref)
            ref = get_target_name(ref)
            references.append(ref)
        column_names = get_target_columns(column_names)
        return ForeignKeyConstraint(column_names, references)

    def _get_target_fkeys(self, table):
        fkeys = dict()
        for c in table.constraints:
            if c.__class__ != ForeignKeyConstraint:
                continue
            refs = [e.target_fullname for e in c.elements]
            t = refs[0].split('.')[:-1]
            ref_table = t[-1]
            ref_schema = t[1:] and t[0] or None
            TRef = self.db_target.get_table(ref_table, ref_schema)
            ref_columns = [column.split('.')[-1] for column in refs]
            ref_pkeys = get_primary_key(TRef)
            ref_table_full = '.'.join(t)
            fkeys[ref_table_full] = (c.columns, ref_columns, TRef,
                                     ref_pkeys)
            print_debug(' fkey {ref_t} {f} {ref_f}, pkeys {pk}'.format(
                ref_t=ref_table_full, f=c.columns, ref_f=ref_columns,
                pk=ref_pkeys))
        return fkeys

    def _fkey_data(self, fkeys, data):
        for fkey in foreign_keys:
            vals = []
            where = dict()
            fnum = -1
            for fname in fkey['sql_fields']:
                fnum += 1
                val = data[fname]
                vals.append(val)
                ref_fname = fkey['ref_fields'][fnum]
                where[ref_fname] = val
            ref_table = fkey['ref_table']
            columns, ref_columns, TRef, pkeys = fkeys[ref_table]
            print_debug('source {t} where {w}'.format(t=ref_table, w=where))
            q = self.db_target.query(TRef).filter_by(**where)
            row_ref = q.first()
            if not row_ref:
                raise Exception('foreign values not found: '\
                    'table {t} {w}'.format(t=ref_table, w=where))
            row_ref = to_dict(row_ref)
            for pkey in pkeys:
                print_debug('pkey val {v}'.format(v=row_ref[pkey]))
            fnum = -1
            for column in columns:
                fnum += 1
                pkey = ref_columns[fnum]
                pkey_val = row_ref[pkey]
                data[column] = pkey_val
            print_debug('data {d}'.format(d=data))
            for fname in fkey['sql_fields']:
                del data[fname]
        return data

    def _get_count(self, options):
        if 'sql_count' not in options:
            return None, None
        with open(options['sql_count']) as f:
            sql_count = f.read()
        count = self.db_source.first_value(sql_count)
        q = self.db_source.execute(sql_count)
        row = q.first()
        count = row[0]
        s_count = ' / {c}'.format(c=thousand(count))
        return count, s_count

    def _load_event(self, options):
        if 'plugin' not in options:
            return
        name = options['plugin']
        if name in self.plugin_names:
            i = self.plugin_names.index(name)
            plugin = self.plugins[i]
        else:
            plugin = load_plugin(name)
            self.plugin_names.append(name)
            self.plugins.append(plugin)
            try:
                setup = getattr(plugin, 'setup')
            except AttributeError:
                setup = None
            setup and setup(self)
        try:
            before_commit = getattr(plugin, 'before_commit')
        except AttributeError:
            before_commit = None
        return before_commit

    def _insert(self, source, target, before_commit):
        try:
            self.db_target.flush(target)
            if before_commit:
                before_commit(self, source, target)
        except IntegrityError, err:
            self.duplicate_key_error(err)
        self.db_target.session.commit()

    def copy_data_from_sql(self, sql, tablename, foreign_keys=[], options={}):
        print('{sql} -> {t}'.format(sql=sql, t=tablename))
        schema_target, tablename_target = split_tablename(tablename)
        table = self.db_target.get_table(tablename_target, schema_target)
        fkeys = foreign_keys and self._get_target_fkeys(table)
        class Target(Base, BaseModel):
            __table__ = table
        before_commit = self._load_event(options)
        count, s_count = self._get_count(options)
        if count == 0:
            print('0 rows.')
            return
        i = log_row = 0
        sources = self.db_source.execute(sql.strip())
        begin_time = time()
        for source in sources:
            i += 1
            log_row += 1
            data = get_dict(source)
            if foreign_keys:
                data = self._fkey_data(fkeys, data)
            target = Target()
            target.from_dict(data)
            self._insert(source, target, before_commit)
            if i > 1000 and log_row < 100:
                continue
            if log_row == 100:
                log_row = 0
            duration = time() - begin_time
            speed = duration / i
            estimate = (count - i) * speed
            s_estimate = humanize_time(estimate)
            print('{t} #{row}{c} {s} second / row estimate {e}'.format(
                t=tablename, row=thousand(i), s=float_thousand(speed),
                c=s_count, e=s_estimate))

    def insert(self, sql):
        try:
            self.db_target.execute(sql)
        except IntegrityError, err:
            self.duplicate_key_error(err)

    def duplicate_key_error(self, err):
        err_ = str(err).lower()
        found = False
        for ref_err in conf.duplicate_key_errors:
            if err_.find(ref_err) > -1:
                self.db_target.session.rollback()
                return
        raise(err)


conf_file = 'conf.py'
pars = OptionParser()
pars.add_option('-c', '--conf', default=conf_file,
        help='configuration file, default: ' + conf_file)
pars.add_option('', '--debug', action='store_true')
option, remain = pars.parse_args(sys.argv[1:])

debug = option.debug
conf_file = option.conf
conf = imp.load_source('conf', conf_file)

locale.setlocale(locale.LC_ALL, conf.locale_format)

db_source = DBProfile(conf.db_url_source)
db_target = DBProfile(conf.db_url_target)

bmove = BatchMove(db_source, db_target, conf.schema_source, conf.schema_target)
for tablename in conf.copy_tablenames:
    bmove.copy(tablename)

for t in conf.copy_sql:
    sql_file = t[0]
    tablename = t[1]
    foreign_keys = t[2:] and t[2] or []
    options = t[3:] and t[3] or dict() 
    f = open(sql_file)
    sql = f.read().replace('\n', ' ')
    f.close()
    bmove.copy_data_from_sql(sql, tablename, foreign_keys, options)
