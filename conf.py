db_url_source = 'oracle://user:pass@192.168.1.2:1521/db'
db_url_target = 'postgresql://user:pass@localhost:5432/db'
schema_source = None
schema_target = None 

limit = 100
lowercase_target = False

# UTF-8 friendly
replace_chars = [
    # ('\xa0', ' '),
    # ('\xb2', ' '),
    ]
printable_chars_only = True

# When there are errors while the insert then we need to know what a meaningful
# error message duplicate key.
duplicate_key_errors = ['duplicate key']

# Nice log 
locale_format = 'id_ID.UTF-8'

copy_tablenames = (
    'department',
    'employee',
    )

copy_sql = [ 
    # ('query1.sql', 'tablename1'),
    # ('query2.sql', 'tablename2'),
    # ('employee.sql', 'hrd.employee',
    #    [
    #    dict(
    #        sql_fields=['dept_code'],
    #        ref_table='hrd.department',
    #        ref_fields=['code'],
    #        ),
    #    dict(
    #        sql_fields=['category_code'],
    #        ref_table='hrd.employee_category',
    #        ref_fields=['code'],
    #        ),
    #    ],
    #    dict(plugin='employee_constraint.py'),
    #    )
    ]
